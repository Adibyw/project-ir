import requests
import ast
import json

base_url = "https://developers.zomato.com/api/v2.1/"


def initialize_app(config):
	return Zomato(config)


class Zomato:
	def __init__(self, config):
		self.user_key = config["user_key"]

	def get_city_ID(self, city_name):
		"""
		Takes City Name as input.
		Returns the ID for the city given as input.
		"""
		if city_name.isalpha() == False:
			return False

		city_name = city_name.split(' ')
		city_name = '%20'.join(city_name)
		headers = {'Accept': 'application/json', 'user-key': self.user_key}
		r = (requests.get(base_url + "cities?q=" + city_name, headers=headers).content).decode("utf-8")
		a = ast.literal_eval(r)

		self.is_key_invalid(a)
		self.is_rate_exceeded(a)

		if len(a['location_suggestions']) == 0:
			return False
		elif 'name' in a['location_suggestions'][0]:
			city_name = city_name.replace('%20', ' ')
			if str(a['location_suggestions'][0]['name']).lower() == str(city_name).lower():
				return a['location_suggestions'][0]['id']
			else:
				return False

	def get_restaurant(self, restaurant_ID):
		"""
		Takes Restaurant ID as input.
		Returns a dictionary of restaurant details.
		"""
		self.is_valid_restaurant_id(restaurant_ID)

		headers = {'Accept': 'application/json', 'user-key': self.user_key}
		r = (requests.get(base_url + "restaurant?res_id=" + str(restaurant_ID), headers=headers).content).decode("utf-8")
		a = ast.literal_eval(r)

		if 'code' in a:
			if a['code'] == 404:
				return False

		restaurant_details = {}
		restaurant_details.update({"name" : a['name']})
		restaurant_details.update({"url" : a['url']})
		restaurant_details.update({"location" : a['location']['address']})
		restaurant_details.update({"city" : a['location']['city']})
		restaurant_details.update({"city_ID" : a['location']['city_id']})
		restaurant_details.update({"user_rating" : a['user_rating']['aggregate_rating']})

		restaurant_details = DotDict(restaurant_details)
		return restaurant_details


	def restaurant_search(self, query="", latitude="", longitude="", cuisines="", limit=""):
		"""
		Takes either query, latitude and longitude or cuisine as input.
		Returns a list of Restaurant IDs.
		"""
		cuisines = "%2C".join(cuisines.split(","))
		headers = {'Accept': 'application/json', 'user-key': self.user_key}
		r = (requests.get(base_url + "search?q=" + str(query) + "&count=" + str(limit) + "&lat=" + str(latitude) + "&lon=" + str(longitude) + "&cuisines=" + str(cuisines), headers=headers).content).decode("utf-8")
		# print(r)
		a = json.loads(r)
		# print(a)

		restaurants = {}

		if a['results_found'] == 0:
			return {}
		else:
			for restaurant in a['restaurants']:
				id_resto = restaurant['restaurant']['id']
				name = restaurant['restaurant']['name']
				city_name = restaurant['restaurant']['location']['city']
				address = restaurant['restaurant']['location']['address']
				cuisine = restaurant['restaurant']['cuisines']
				image = restaurant['restaurant']['featured_image']

				restaurants[id_resto] = {}
				restaurants[id_resto]["name"] = name
				restaurants[id_resto]["city"] = city_name
				restaurants[id_resto]["cuisine"] = cuisine
				restaurants[id_resto]["image"] = image
				restaurants[id_resto]["address"] = address

		return restaurants




	def is_valid_restaurant_id(self, restaurant_ID):
		"""
		Checks if the Restaurant ID is valid or invalid.
		If invalid, throws a InvalidRestaurantId Exception.
		"""
		restaurant_ID = str(restaurant_ID)
		if restaurant_ID.isnumeric() == False:
			raise ValueError('InvalidRestaurantId')



	def is_valid_city_id(self, city_ID):
		"""
		Checks if the City ID is valid or invalid.
		If invalid, throws a InvalidCityId Exception.
		"""
		city_ID = str(city_ID)
		if city_ID.isnumeric() == False:
			raise ValueError('InvalidCityId')



	def is_key_invalid(self, a):
		"""
		Checks if the API key provided is valid or invalid.
		If invalid, throws a InvalidKey Exception.
		"""
		if 'code' in a:
			if a['code'] == 403:
				raise ValueError('InvalidKey')



	def is_rate_exceeded(self, a):
		"""
		Checks if the request limit for the API key is exceeded or not.
		If exceeded, throws a ApiLimitExceeded Exception.
		"""
		if 'code' in a:
			if a['code'] == 440:
				raise Exception('ApiLimitExceeded')



class DotDict(dict):
	"""
	Dot notation access to dictionary attributes
	"""

	__getattr__ = dict.get
	__setattr__ = dict.__setitem__
	__delattr__ = dict.__delitem__
