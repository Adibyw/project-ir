from django.conf.urls import url
from .views import index, search

#url for app
urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^search/', search, name='search'),
]