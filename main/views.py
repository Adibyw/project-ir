from django.shortcuts import render
from django.http import HttpResponseRedirect
from .zomatopy import *
from .models import Restaurant

config={
  "user_key":"490a023a1e12e898ba70c7bfd3715138"
}

response = {}

# Create your views here.
def index(request):
	html = "landing_page.html"
	return render(request, html, response)

def search(request):
	if request.method == "POST":
		makanan = request.POST.get('food')
		query = makanan.lower().split(" ")

		if Restaurant.objects.all():
			Restaurant.objects.all().delete()
	

		zomato = initialize_app(config)
		cuisine_not_found = False

		list_resto = {}
		ranked = []

		list_resto = zomato.restaurant_search(makanan)

		if list_resto == False or len(list_resto) == 0:
			cuisine_not_found = True
		else:
			ranked = rank_resto(list_resto, query)

		createModel(ranked, list_resto)

		response["cuisine_not_found"] = cuisine_not_found
		response["list_resto"] = Restaurant.objects.all()
				
	return render(request, "result.html", response)


def rank_resto(list_resto, query):
	rank_dict = {}
	rank = []

	for resto_id in list_resto:
		rank_dict[resto_id] = score(list_resto[resto_id], query)

	sort = sorted(rank_dict.items(), key=lambda kv: kv[1])

	for resto in sort:
		rank.append(resto[0])

	return rank

def score(detail_resto, query):
	score = 0
	name = detail_resto["name"].lower().split(" ")
	cuisine = detail_resto["cuisine"].lower().split(", ")

	jmlh_nama_sama = 0
	for kata in name:
		if kata in query:
			# score jika query ada di dalam nama restauran += 0.7
			jmlh_nama_sama += 1

	jmlh_cuisine_sama = 0
	jmlh_cuisine_beda = 0
	for kata in cuisine:
		if kata in query:
			# score jika query ada di dalam cuisine restauran += 0.5
			jmlh_cuisine_sama += 1
		else:
			# score jika cuisine restauran punya hubungan dengan query += 0.1
			jmlh_cuisine_beda += 1

	score += jmlh_nama_sama * 0.7
	score += jmlh_cuisine_sama * 0.5
	score += jmlh_cuisine_beda * 0.1

	return score

def createModel(ranked, list_resto):
	for resto in ranked:
		nama = list_resto[resto]["name"]
		city = list_resto[resto]["city"]
		cuisine = list_resto[resto]["cuisine"]
		address = list_resto[resto]["address"]
		image = list_resto[resto]["image"]

		obj = Restaurant(idResto=resto, name=nama, city=city, cuisine=cuisine, image=image, address=address)
		obj.save()