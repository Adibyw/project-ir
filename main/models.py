from django.db import models

# Create your models here.

class Restaurant(models.Model):
	idResto = models.CharField(max_length=40, default=0)
	name = models.CharField(max_length=1000, default=0)
	city = models.CharField(max_length=100, default=0)
	cuisine = models.CharField(max_length=2000, default=0)
	address = models.CharField(max_length=2000, default=0)
	image = models.CharField(max_length=2000, default=0)
